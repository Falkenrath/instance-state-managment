from os import environ
from boto3 import client, resource
from operator import attrgetter

class AWSConnections:
    def __init__(self, 
                 service, 
                 region, 
                 service_client= None, 
                 service_resource= None,
                 aws_access_key_id= None,
                 aws_secret_access_key= None
                 ):
        self.service = service
        self.region = region
        if service_client is None:
            service_client =  client(service, region_name= region, aws_access_key_id= aws_access_key_id, aws_secret_access_key= aws_secret_access_key)
        if service_resource is None:
            service_resource = resource(service, region_name= region, aws_access_key_id= aws_access_key_id, aws_secret_access_key= aws_secret_access_key)
 
        self.service_client = service_client
        self.service_resource = service_resource


class EC2Management(AWSConnections):

    def __init__(self, region, service_client=None, service_resource=None, aws_access_key_id=None, aws_secret_access_key=None):
        super().__init__(service='ec2', region=region, service_client=service_client, service_resource=service_resource, aws_access_key_id= aws_access_key_id, aws_secret_access_key= aws_secret_access_key)
        
    
    # Takes a list of filters
    def get_all_instances(self, fil=None):
        instances = []
        [instances.append(instance) for reservation in self.service_client.describe_instances(Filters=fil)['Reservations']
         for instance in reservation['Instances']
        ]
        return instances
    
        
    def get_attr_from_instances(self, instances, attr):
        getter = lambda itr, value: [i.get(value) for i in itr ]
        return getter(instances, attr)
        


    def list_instance_tag_names(self):
        instances = self.get_all_instances()
        
        keys = [ tag['Key']
          for tags in self.get_attr_from_instances(self.get_all_instances(), 'Tags')
          for tag in tags
        ]
        return set(keys)
     


    def get_ids_from_tag(self, tag_name, tag_values):
        instance_filter = [{'Name': f'tag:{tag_name}', 'Values': tag_values}]

        instances = self.get_all_instances(instance_filter)
        instance_ids = self.get_attr_from_instances(instances,'InstanceId')
        return instance_ids